# OMiCleanDedupEvents: list events ID, and few other relevant info of events that were missed by OMi deduplication mechanism. Optionnally it can close those events.
OMiCleanDedupEvents is built based on OMi REST CLIs.
OMiCleanDedupEvents lists events ID, along with few other relevant info (TIME_CREATED, NODE_HINTS, etc…) of events that were missed by OMi deduplication mechanism. Optionally it can close those events. All the events that should have been marked as duplicate but the latest one for each correlation keys are listed. This allows for a quick closing of the events from the output.
It finds the duplicates based on the correlation keys as this one is supposedly be unique.


[![MIT license](https://img.shields.io/badge/license-MIT-blue.svg)](https://github.com/vi7Ch/Test/LICENSE)
[![built with Groovy2.8](https://img.shields.io/badge/built%20with-Groovy-red.svg)](http://www.groovy-lang.org)
[![built with Java8](https://img.shields.io/badge/built%20with-Java-green.svg)](https://www.java.com/en/)

> **DISCLAIMER**
    THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
    HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


### Version: 0.1 - 28/12/2017: initial release - Viseth Chaing
### Version: 0.2 - 02/01/2018: update the printview of events ID - Viseth Chaing
### Version: 0.3 - 03/01/2018: anywhere execution, the new OMiCleanDedupEvents.jar can be run outside a DPS server provide the execution host has remote access to OMi database and the OMi Web URL - Viseth Chaing
### Version: 0.4 - 27/02/2018: allows selecting given from and to datetime - Viseth Chaing


Current supported environment: OMi Windows/Linux - Oracle DB 

> **Deployment on OMi DPS context** 
- Download the **OMiCleanDedupEvents_local_DPS.jar**, **close.xml** and **config_local_DPS.cfg** files. 
- Put them in one OMi DPS management servers in a safe directory. 
- Modify according to your OMi environment the default **config_local_DPS.cfg** and rename it to **config.cfg**. 


> **Deployment anywhere context** 
- Download the **OMiCleanDedupEvents.jar**, **close.xml** and **config.cfg** files, and the **lib** folder 
- Put them in a host server which has remote access to OMi database and the OMi Web URL. 
- Modify according to your OMi environment the default **config.cfg** . 



### Manual Usage:
Go to the directory where you downloaded the files and folder **lib**, and open a windows or shell command from there.

The default named 'config.cfg' file in the current directory is used and a 'logs' directory is created with the logfile.
Keep the heapsize low

```bash
java -Xmx128m -jar OMiCleanDedupEvents.jar
```


The default behaviour is to print only the eventID. If the option **close=true** is set, those events will be closed as well. 

```bash
java -Xmx128m -Dclose=true -jar OMiCleanDedupEvents.jar
```


To select between 2 given datetime fom and to (default to='now'). 2 options: if **from="2018-02-25 15:30:00"** is set, selected events where time received is greater than 'from', and optionally **to="2018-02-26 16:30:00"** is set, selected events where time received is less than 'to'

```bash
java -Xmx128m -Dfrom="2018-02-25 15:30:00" -Dto="2018-02-26 16:30:00" -jar OMiCleanDedupEvents.jar
```


To select between 2 given datetime fom and to (default to='now') and close them.

```bash
java -Xmx128m -Dclose=true -Dfrom="2018-02-25 15:30:00" -Dto="2018-02-26 16:30:00" -jar OMiCleanDedupEvents.jar
```


To encrypt the password (database pwd), copy/paste the output to the config.cfg file in place of the 'pwd' field

```bash
java -jar -DpwdEncrypt=<myNewPwd> OMiCleanDedupEvents.jar
``` 


Check and result:

The output of the tool tells you that the underline eventID did not end up as a duplicate while it should (thus could be closed)

![](images/omi_events.png)


And as you can see the latest event was stripped from the output, so that any occurences of the event with the same correlation keys are printed out.

![](images/output.png)

### Note:
**Required INPUT files**
- A CONFIG file named 'config.cfg' with correct info inside


**OUTPUT files**
- List events ID, and few other relevant info of events that were missed by OMi deduplication mechanism.
- Logfiles: this tool logs its events in a logfile with the below caracteristics: 

```bash
        - LogLevel: INFO, ERROR, DEBUG
        - LogSize: 10MB
        - LogRotate: 5 files
        - Directory of the logfile: 'logs' 
        - Name: 'OMiCleanDedupEvents.log'

To turn debugging mode so that debugging lines will appear in the logfile:
java -Xmx128m -Dapp.env=DEBUG -jar OMiCleanDedupEvents.jar

```
A good practice would be to check duplicate misses every morning.

